class CreateItems < ActiveRecord::Migration[5.1]
  def change
    create_table :items do |t|
      t.string :fecha
      t.string :title
      t.string :image
      t.string :content
      t.string :all_content
      t.string :link

      t.timestamps
    end
  end
end
