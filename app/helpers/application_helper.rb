module ApplicationHelper
 
  require 'uri'
  require 'net/http'
  require 'json'
  
  def get_data_json(url)
    url = url.gsub(" ","")
    escaped_address = URI.escape(url) 
    uri = URI.parse(url)

    if uri.kind_of?(URI::HTTP) or uri.kind_of?(URI::HTTPS)
      result = Net::HTTP.get_response(uri)
      if valid_json?(result.body)
        result = JSON.parse(result.body)
      else  
        return false  
      end
      if !result.nil? && !result['items'].nil? 
        result['items'].each do |item|
          i = Item.create(fecha: item['fecha'], title: item['title'], image: item['image'], content: item['content'], all_content: item['all_content'],  link: item['link'])
          !item['comments'].nil? ? Comment.create(content: item['comments'], item_id: i.id) : ''
          if !item['categories'].nil? 
            item['categories'].each do |cat| ## validar esto 
              c = Category.where(name: cat['name'] )
              if c.empty?
                c = Category.create(name: cat['name'], slug: cat['slug'])
                !i.errors.messages.any? ?  CategoriesItem.create( category_id: c.id, item_id: i.id) : ''       
              else
                !i.errors.messages.any? ?  CategoriesItem.create( category_id: c[0].id, item_id: i.id) : ''
              end
            end
          end
        end
      end
    end
  end

  def valid_json?(string)
    begin
      !!JSON.parse(string)
    rescue JSON::ParserError
      false
    end
  end
end
