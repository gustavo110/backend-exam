class Comment < ApplicationRecord
  belongs_to :item
  belongs_to :user, optional: true
  has_many :likes, dependent: :destroy
  validates :content , :item, presence: true
end
