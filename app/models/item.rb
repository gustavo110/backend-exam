class Item < ApplicationRecord
  has_and_belongs_to_many :categories
  has_many :comments, dependent: :destroy
  validates :title , presence: true, uniqueness: true
  validates :content, presence: true
end
