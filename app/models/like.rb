class Like < ApplicationRecord
  belongs_to :comment
  belongs_to :user, optional: true
  validates :comment , presence: true
  before_save :duplicate_like 

  def duplicate_like
    !self.user_id.nil? && Like.where( user_id: self.user_id, comment_id: self.comment_id).any? ? throw(:abort) : ''
  end
end
