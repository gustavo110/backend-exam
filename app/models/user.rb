class User < ApplicationRecord
  has_many :comments
  has_many :likes, dependent: :destroy
end
