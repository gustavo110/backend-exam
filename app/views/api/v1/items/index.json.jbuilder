json.array!(@items) do |i|
  json.item do
    json.extract! i, :fecha, :title, :image, :content, :all_content, :link
    json.comments do 
      json.array!(i.comments) do |c|
        json.extract! c, :content
        json.likes c.likes.size
      end
    end
  end
end