class UsersDatatable
  delegate :params, :h, :link_to, to: :@view

  include Rails.application.routes.url_helpers
  include ActionView::Helpers::OutputSafetyHelper
  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
      sEcho: params[:sEcho].to_i,
      iTotalRecords: User.count,
      iTotalDisplayRecords: users.total_entries,
      aaData: data
    }
  end

  private

  def data
    users.map do |user|
      [
        user.id,
        user.name,
      ]
    end
  end

  def users
    @users ||= fetch_users
  end

  def fetch_users
    users = User.all.order("#{sort_column} #{sort_direction}")
    users = users.page(page).per_page(per_page)
    if params[:sSearch].present?
      users = users.where("users.name ILIKE :search", search: "%#{params[:sSearch]}%")
    end
    users
  end

  def page
    params[:iDisplayStart].to_i / per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = ['users.id','users.name']
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0] == 'desc' ? 'desc' : 'asc'
  end
end
