class HomeController < ApplicationController
  
  include ApplicationHelper
  
  def index
  end

  def new_data
  	get_data_json(params['url'])
    redirect_to root_path
  end
  
  def instructions
    
  end
end
