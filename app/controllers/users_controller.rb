class UsersController < ApplicationController
  
  def index
    respond_to do |format|
      format.html
      format.json { render json: UsersDatatable.new(view_context) }
    end
  end

  def create
    User.create(name: Faker::Name.name)
    redirect_to root_path
  end

end
