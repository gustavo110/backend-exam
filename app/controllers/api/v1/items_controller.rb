module Api
  module V1
    class ItemsController < V1Controller
      def index
        @items = Item.all
      end
    end
  end
end
  