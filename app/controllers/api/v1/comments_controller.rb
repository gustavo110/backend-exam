module Api
  module V1
    class CommentsController < V1Controller

      def create
        @comment = Comment.new(comment_params)
        if @comment.save
          @status = :created
        else
          @status = :bad_request
        end
        render :create, status: @status
      end

      private
      def comment_params
        params.require(:comment)
              .permit(:content, :user_id, :item_id)
      end
    end
  end
end
