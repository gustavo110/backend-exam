module Api
  module V1
    class LikesController < V1Controller
      def create
        @like = Like.new(like_params)
        if @like.save
          @status = :created
        else
          @status = :bad_request
        end
        render status: @status
      end

      private
      def like_params
        params.require(:like)
              .permit(:comment_id, :user_id)
      end
    end
  end
end
