Rails.application.routes.draw do
  root 'home#index'
  post 'new_data', to: 'home#new_data'
  get 'instructions', to: 'home#instructions'
  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :items, only: [:index]
      resources :comments, only: [:create]
      resources :likes, only: [:create]
    end
  end
  resources :users, only: [:create, :index]
end
